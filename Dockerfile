ARG EXTRA
ARG OPENCV_VERSION=3.4.11

ARG BASE=alpine:edge
FROM ${BASE} AS base

RUN echo -e 'https://dl-cdn.alpinelinux.org/alpine/edge/testing\nhttps://dl-cdn.alpinelinux.org/alpine/edge/community\n' >> /etc/apk/repositories

# install base stuff
RUN apk add --no-cache python3 py3-numpy py3-pip py3-pillow ffmpeg zlib libpng tzdata

# Install tools and dependencies
RUN apk add --update --no-cache --virtual .build-deps \
      build-base \
      bash \
      ffmpeg-dev \
      openblas-dev \
      python3-dev \
      py3-numpy-dev \
      unzip \
      wget \
      cmake \
      #Intel® TBB, a widely used C++ template library for task parallelism'
      libtbb \
      libtbb-dev \
      # Wrapper for libjpeg-turbo
      libjpeg  \
      # accelerated baseline JPEG compression and decompression library
      libjpeg-turbo-dev \
      # Portable Network Graphics library
      libpng-dev \
      # jasper-dev is replaced by this
      openjpeg-dev \
      # Provides support for the Tag Image File Format or TIFF (development files)
      tiff-dev \
      # needed for sfm (opencv_contrib)
      eigen-dev \
      gflags-dev \
      glog-dev \
      # Libraries for working with WebP images (development files)
      libwebp-dev \
      # A C language family front-end for LLVM (development files)
      clang-dev \
      linux-headers


ENV CC /usr/bin/clang
ENV CXX /usr/bin/clang++


RUN mkdir -p /opt && cd /opt && \
  wget -q https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip && \
  unzip ${OPENCV_VERSION}.zip && \
  rm -rf ${OPENCV_VERSION}.zip

RUN mkdir -p /opt/opencv-${OPENCV_VERSION}/build && \
  cd /opt/opencv-${OPENCV_VERSION}/build && \
  cmake \
  -D CMAKE_BUILD_TYPE=RELEASE \
  -D CMAKE_INSTALL_PREFIX=/usr \
  -D WITH_FFMPEG=YES \
  -D WITH_IPP=NO \
  -D WITH_OPENEXR=NO \
  -D WITH_TBB=YES \
  -D BUILD_EXAMPLES=NO \
  -D BUILD_ANDROID_EXAMPLES=NO \
  -D INSTALL_PYTHON_EXAMPLES=NO \
  -D BUILD_DOCS=NO \
  -D BUILD_opencv_python2=NO \
  -D BUILD_opencv_python3=ON \
  -D PYTHON3_EXECUTABLE=/usr/bin/python3 \
  -D PYTHON3_INCLUDE_DIR=$(python3 -c 'import sysconfig;print(sysconfig.get_path("include"))') \
  -D PYTHON3_LIBRARY=/usr/lib/libpython3.so \
  -D PYTHON3_PACKAGES_PATH=$(python3 -c 'import sysconfig;print(sysconfig.get_path("purelib"))') \
  -D PYTHON3_NUMPY_INCLUDE_DIRS=$(python3 -c 'import numpy;print(numpy.get_include())') \
  ..

FROM base as basecontrib
RUN cd /opt/opencv-${OPENCV_VERSION}/ && \
  wget -q https://github.com/opencv/opencv_contrib/archive/${OPENCV_VERSION}.zip && \
  unzip ${OPENCV_VERSION}.zip && \
  rm -rf ${OPENCV_VERSION}.zip

RUN mkdir -p /opt/opencv-${OPENCV_VERSION}/build && \
  cd /opt/opencv-${OPENCV_VERSION}/build && \
  cmake \
  -D CMAKE_BUILD_TYPE=RELEASE \
  -D CMAKE_INSTALL_PREFIX=/usr \
  -D OPENCV_EXTRA_MODULES_PATH=/opt/opencv-${OPENCV_VERSION}/opencv_contrib-${OPENCV_VERSION}/modules \
  -D WITH_FFMPEG=YES \
  -D WITH_IPP=NO \
  -D WITH_OPENEXR=NO \
  -D WITH_TBB=YES \
  -D BUILD_EXAMPLES=NO \
  -D BUILD_ANDROID_EXAMPLES=NO \
  -D INSTALL_PYTHON_EXAMPLES=NO \
  -D BUILD_DOCS=NO \
  -D BUILD_opencv_python2=NO \
  -D BUILD_opencv_python3=ON \
  -D PYTHON3_EXECUTABLE=/usr/bin/python3 \
  -D PYTHON3_INCLUDE_DIR=$(python3 -c 'import sysconfig;print(sysconfig.get_path("include"))') \
  -D PYTHON3_LIBRARY=/usr/lib/libpython3.so \
  -D PYTHON3_PACKAGES_PATH=$(python3 -c 'import sysconfig;print(sysconfig.get_path("purelib"))') \
  -D PYTHON3_NUMPY_INCLUDE_DIRS=$(python3 -c 'import numpy;print(numpy.get_include())') \
  .. 


FROM base${EXTRA} as final
RUN cd /opt/opencv-${OPENCV_VERSION}/build && \
  make -j$(nproc) && \
  make install

RUN cd / && rm -rf /opt/opencv-${OPENCV_VERSION}
RUN apk del .build-deps